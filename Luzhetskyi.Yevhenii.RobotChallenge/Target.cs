﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Luzhetskyi.Yevhenii.RobotChallenge
{
    class Target
    {
        private bool Active { get; set; }
        private Position TargetPosition { get; set; }
        //private List<Position> MoveList { get; set; }

        public Target() {}

        public Target(Position currentPosition, Position targetPosition, bool active = false)
        {
            Active = active;
            TargetPosition = targetPosition;
        }

        /// <summary>
        /// Set target and build a route
        /// </summary>
        /// <param name="startPosition"></param>
        /// <param name="targetPosition"></param>
        /// <param name="map"></param>
        /// <param name="stepMaxDistance"></param>
        /// <param name="active"></param>
        /// <returns>Energy needed to move</returns>
        /*public int GetRequiredEnergy(Position startPosition,
                                     IList<Robot.Common.Robot> robots,
                                     int stepMaxDistance)
        {
            var currentPosition = startPosition;
            var targetDistance = Utilities.CalculateDistance(startPosition, TargetPosition);
            var totalEnergy = 0;
            bool positiveOffsetX = (TargetPosition.X - startPosition.X > 0);
            bool positiveOffsetY = (TargetPosition.Y - startPosition.Y > 0);
            MoveList.Clear();
            
            while (Math.Abs(targetPosition.X - currentPosition.X) >= stepMaxDistance)
            {
                totalEnergy += (stepMaxDistance * stepMaxDistance);
                currentPosition.X += (positiveOffsetX ? stepMaxDistance : -stepMaxDistance);
                MoveList.Add(currentPosition);
            }
            
            if (targetPosition.X - currentPosition.X != 0)
            {
                int offset = Math.Abs(targetPosition.X - currentPosition.X);
                currentPosition.X += (targetPosition.X - currentPosition.X);

            }
            while (targetDistance > stepMaxDistance)
            {
                
            }

            if (targetDistance != 0)
            {
                totalEnergy += Utilities.CalculateEnergy(currentPosition, targetPosition);
                MoveList.Add(targetPosition);
            }

            return totalEnergy;
        }*/

        /*public Position Move()
        {
            if (MoveList.Count == 0) throw new Exception("No target to move");
            Position res = MoveList.First();
            MoveList.RemoveAt(0);
            return res;
        }*/

        /*
        /// <summary>
        /// Set target and build a route
        /// </summary>
        /// <param name="startPosition"></param>
        /// <param name="targetPosition"></param>
        /// <param name="map"></param>
        /// <param name="stepMaxDistance"></param>
        /// <param name="active"></param>
        /// <returns>Energy needed to move</returns>
        public int SetTarget(Position startPosition,
                              Position targetPosition,
                              Map map,
                              int stepMaxDistance,
                              bool active = false)
        {
            var currentPosition = startPosition;
            var targetDistance = Utilities.CalculateDistance(startPosition, targetPosition);
            var totalEnergy = 0;
            bool positiveOffsetX = (targetPosition.X - startPosition.X > 0);
            bool positiveOffsetY = (targetPosition.Y - startPosition.Y > 0);
            Active = active;
            MoveList.Clear();

            while (Math.Abs(targetPosition.X - currentPosition.X) >= stepMaxDistance)
            {
                totalEnergy += (stepMaxDistance * stepMaxDistance);
                currentPosition.X += (positiveOffsetX ? stepMaxDistance : -stepMaxDistance);
                MoveList.Add(currentPosition);
            }

            if (targetPosition.X - currentPosition.X != 0)
            {
                int offset = Math.Abs(targetPosition.X - currentPosition.X);
                currentPosition.X += (targetPosition.X - currentPosition.X);

            }
            while (targetDistance > stepMaxDistance)
            {

            }

            if (targetDistance != 0)
            {
                totalEnergy += Utilities.CalculateEnergy(currentPosition, targetPosition);
                MoveList.Add(targetPosition);
            }

            return totalEnergy;
        }

        public Position Move()
        {
            if (MoveList.Count == 0) throw new Exception("No target to move");
            Position res = MoveList.First();
            MoveList.RemoveAt(0);
            return res;
        }
        */
    }
}
