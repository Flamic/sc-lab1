﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Luzhetskyi.Yevhenii.RobotChallenge
{
    public class MyRobot
    {
        public Robot.Common.Robot Robot => new Robot.Common.Robot
        {
            Energy = Energy,
            OwnerName = (string)OwnerName.Clone(),
            Position = Position.Copy()
        };
        public Robot.Common.Robot FakeRobot => new Robot.Common.Robot
        {
            Energy = Energy - EnergyLoss,
            OwnerName = (string)OwnerName.Clone(),
            Position = Target != null ? Target.Copy() : Position.Copy()
        };

        public Position Position { get; private set; }
        public int Energy { get; set; }
        public string OwnerName { get; }
        public Position Target { get; set; } = null;
        public bool OnTarget => Position == Target;
        public int EnergyLoss => Target == null ? 0 : Utilities.CalculateEnergy(Position, Target);

        public MyRobot()
        {
            Position = new Position(0, 0);
            Energy = 0;
            OwnerName = "Undefined";
        }

        public MyRobot(Robot.Common.Robot robot)
        {
            if (robot == null)
            {
                Position = new Position(0, 0);
                Energy = 0;
                OwnerName = "Undefined";
            }
            else
            {
                Position = robot.Position.Copy();
                Energy = robot.Energy;
                OwnerName = (string) robot.OwnerName.Clone();
            }
        }

        public MoveCommand MoveToTarget(Position target = null)
        {
            if (target != null) Target = target.Copy();
            if (Target == null || EnergyLoss >= Energy) return null;
            Energy -= EnergyLoss;
            Position = Target;
            return new MoveCommand { NewPosition = Position };
        }
    }
}
