﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Luzhetskyi.Yevhenii.RobotChallenge
{
    public class Utilities
    {
        public static int Sqr(int num)
        {
            return num * num;
        }

        public static int Min2D(int x1, int x2)
        {
            return new int[]
            {
                Sqr(x1 - x2),
                Sqr(x1 - x2 + 100),
                Sqr(x1 - x2 - 100)
            }.Min();
        }
        
        public static int CalculateEnergy(Position p1, Position p2)
        {
            return Min2D(p1.X, p2.X) + Min2D(p1.Y, p2.Y);
        }

        public static double CalculateDistance(Position a, Position b)
        {
            return Math.Sqrt(CalculateEnergy(a, b));
        }

        /*
        public static List<EnergyStation> FindStations(Map map, Position position, int radius)
        {
            return map.Stations.Where(station =>
                (Math.Abs(station.Position.X - position.X) <= radius 
                    || Math.Abs(station.Position.X - position.X - map.MaxPozition.X) <= radius
                    || Math.Abs(station.Position.X - position.X + map.MaxPozition.X) <= radius)
                && (Math.Abs(station.Position.Y - position.Y) <= radius
                    || Math.Abs(station.Position.X - position.Y - map.MaxPozition.Y) <= radius
                    || Math.Abs(station.Position.X - position.Y + map.MaxPozition.Y) <= radius)
                ).ToList();
        }
        */

        public static List<EnergyStation> FindStations(Map map, Position position, int radius)
        {
            return map.Stations.Where(station => Math.Abs(station.Position.X - position.X) <= radius
                                                 && Math.Abs(station.Position.Y - position.Y) <= radius).ToList();
        }

        public static int GetAvailableEnergy(Map map, Position position, int radius, int maxEnergy)
        {
            return FindStations(map, position, radius).Sum(
                station => station.Energy > maxEnergy ? maxEnergy : station.Energy
            );
        }

        public static int[,] GetStationsCoverageMap(Map map, int radius)
        {
            var coverageMap = new int[map.MaxPozition.Y, map.MaxPozition.X];
            foreach (var station in map.Stations)
            {
                for (var xOffset = -radius; xOffset <= radius; ++xOffset)
                for (var yOffset = -radius; yOffset <= radius; ++yOffset)
                    if (map.IsValid(new Position(station.Position.X + xOffset, station.Position.Y + yOffset)))
                        ++coverageMap[station.Position.Y + yOffset, station.Position.X + xOffset];
            }

            return coverageMap;
        }

        //====== ATTENTION! My robots in the list must be positioned according to their targets!!! ======//
        public static void CalculateEnergyMap(
            Map map,
            IList<Robot.Common.Robot> robots,
            int radius,
            int remainingCollectingRounds,
            int[,] energyMap
        )
        {
            Array.Clear(energyMap, 0, energyMap.Length);
            foreach (var station in map.Stations)
            {
                for (var xOffset = -radius; xOffset <= radius; ++xOffset)
                for (var yOffset = -radius; yOffset <= radius; ++yOffset)
                    if (map.IsValid(new Position(station.Position.X + xOffset, station.Position.Y + yOffset)))
                    {
                        energyMap[station.Position.Y + yOffset, station.Position.X + xOffset] +=
                            station.RecoveryRate + station.Energy / remainingCollectingRounds;
                    }
            }

            foreach (var robot in robots)
            {
                foreach (var station in FindStations(map, robot.Position, radius))
                {
                    for (var xOffset = -radius; xOffset <= radius; ++xOffset)
                    for (var yOffset = -radius; yOffset <= radius; ++yOffset)
                        if (map.IsValid(new Position(station.Position.X + xOffset, station.Position.Y + yOffset)))
                        {
                            energyMap[station.Position.Y + yOffset, station.Position.X + xOffset] -=
                                Data.MaxEnergyCanCollect;
                        }
                }
            }
        }

        public static Position FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map,
            IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (IsStationFree(station, movingRobot, robots))
                {
                    int d = CalculateEnergy(station.Position, movingRobot.Position);
                    if (d < minDistance)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }

            return nearest?.Position;
        }

        public static bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot,
            IList<Robot.Common.Robot> robots)
        {
            return IsCellFree(station.Position, movingRobot, robots);
        }

        public static bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot && robot.Position == cell)
                {
                    return false;
                }
            }

            return true;
        }

        public static bool IsCellFree(Position cell, IList<Robot.Common.Robot> robots)
        {
            return robots.All(robot => robot.Position != cell);
        }
    }
}