﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luzhetskyi.Yevhenii.RobotChallenge
{
    public class Data
    {
        public static readonly int EnergyStationForAttendant = 5;
        public static readonly int MaxEnergyGrowth = 100;
        public static readonly int MinEnergyGrowth = 50;
        public static readonly int MaxStationEnergy = 20000;
        public static readonly int CollectingDistance = 2;
        public static readonly int MaxEnergyCanCollect = 40;
        public static readonly int EnergyLossToCreateNewRobot = 100;
        public static readonly int RoundsCount = 50;

        public const int NoMoveRound = 43;
        public const int NoReplicationRound = 35;
        public const int MaxMoveDistance = 8;
        public const int MaxMoveEnergy = MaxMoveDistance * MaxMoveDistance + 10;
        public const int MinDesiredEnergy = 1;
        public const int CriticalCellEnergy = -250;
        public const int ReplicationEnergy = 250;
    }
}
