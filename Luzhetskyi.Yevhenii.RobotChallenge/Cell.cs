﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Luzhetskyi.Yevhenii.RobotChallenge
{
    public class Cell
    {
        public Position Position { get; set; }
        public int StationsCount { get; set; }
        
        public Cell(Position position)
        {
            this.Position = position.Copy();
        }

        public Cell(int x, int y)
        {
            this.Position = new Position(x, y);
        }

        /*public bool IsAvailable(Map map, List<Robot.Common.Robot> robots, int radius)
        {

        }*/
    }
}
