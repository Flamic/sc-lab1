﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Luzhetskyi.Yevhenii.RobotChallenge
{
    public class LuzhetskyiAlgorithm : IRobotAlgorithm
    {
        public int Round { get; private set; }
        private int[,] CoverageMap { get; set; }
        private int[,] EnergyMap { get; set; } = new int[100, 100];
        private bool Initialized { get; set; } = false;
        private bool MoveMode { get; set; } = true;
        private bool ReplicationMode { get; set; } = true;

        public delegate void CommandHandler(RobotCommand command);
        public event CommandHandler CommandNotifier;

        public LuzhetskyiAlgorithm()
        {
            Logger.OnLogRound += Logger_OnLogRound;
        }
        
        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            switch (++Round)
            {
                case Data.NoReplicationRound:
                    ReplicationMode = false;
                    break;
                case Data.NoMoveRound:
                    MoveMode = false;
                    break;
            }
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            if (!Initialized)
            {
                Initialized = true;
                CoverageMap = Utilities.GetStationsCoverageMap(map, Data.CollectingDistance);
            }

            var movingRobot = robots[robotToMoveIndex];
            var myRobots = robots.Where(robot => robot.OwnerName == Author);
            RobotCommand command;

            Utilities.CalculateEnergyMap(
                map,
                robots,
                Data.CollectingDistance,
                Data.RoundsCount - Round + 1,
                EnergyMap
            );

            if (MoveMode
                && (movingRobot.Energy == 100
                    || EnergyMap[movingRobot.Position.Y, movingRobot.Position.X] < Data.CriticalCellEnergy))
            {
                var bestPosition = GetBestPosition(map, movingRobot, robots);
                if (bestPosition != null && bestPosition != movingRobot.Position)
                {
                    command = new MoveCommand {NewPosition = bestPosition};
                    CommandNotifier?.Invoke(command);
                    return command;
                }
            }

            if (ReplicationMode && myRobots.Count() < 100 && movingRobot.Energy >= Data.ReplicationEnergy)
            {
                var robot = new Robot.Common.Robot
                {
                    Energy = 100,
                    OwnerName = Author,
                    Position = map.FindFreeCell(movingRobot.Position, robots)
                };
                
                if (GetBestPosition(map, robot, robots) != null)
                {
                    command = new CreateNewRobotCommand();
                    CommandNotifier?.Invoke(command);
                    return command;
                }
            }

            command = new CollectEnergyCommand();
            CommandNotifier?.Invoke(command);
            return command;
        }

        private Position GetBestPosition(Map map, Robot.Common.Robot myRobot, IList<Robot.Common.Robot> robots)
        {
            Position bestPosition = null;
            var stationsCount = -1;
            var neededEnergy = Data.MaxMoveEnergy + 1;
            for (var xOffset = -Data.MaxMoveDistance; xOffset <= Data.MaxMoveDistance; ++xOffset)
            for (var yOffset = -Data.MaxMoveDistance; yOffset <= Data.MaxMoveDistance; ++yOffset)
            {
                var position = new Position(myRobot.Position.X + xOffset, myRobot.Position.Y + yOffset);
                if (position.X < 0) position.X += 100;
                else if (position.X >= 100) position.X -= 100;
                if (position.Y < 0) position.Y += 100;
                else if (position.Y >= 100) position.Y -= 100;
                var energy = Utilities.CalculateEnergy(myRobot.Position, position);

                if (map.IsValid(position)
                    && energy <= Data.MaxMoveEnergy
                    && energy < myRobot.Energy
                    && (myRobot.Position == position || Utilities.IsCellFree(position, robots))
                    && (CoverageMap[position.Y, position.X] > stationsCount
                        || (CoverageMap[position.Y, position.X] == stationsCount
                            && energy < neededEnergy))
                    && EnergyMap[position.Y, position.X] >= Data.MinDesiredEnergy)
                {
                    bestPosition = position;
                    stationsCount = CoverageMap[position.Y, position.X];
                    neededEnergy = energy;
                }
            }

            return bestPosition;
        }

        public string Author => "Luzhetskyi Yevhenii";
    }
}