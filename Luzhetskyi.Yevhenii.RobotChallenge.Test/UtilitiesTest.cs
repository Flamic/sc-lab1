﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Robot.Common;

namespace Luzhetskyi.Yevhenii.RobotChallenge.Test
{
    [TestClass]
    public class UtilitiesTest
    {
        [TestMethod]
        public void CalculateEnergyTest1()
        {
            var pos1 = new Position(0, 0);
            var pos2 = new Position(0, 0);
            Assert.AreEqual(0, Utilities.CalculateEnergy(pos1, pos2));
        }

        [TestMethod]
        public void CalculateEnergyTest2()
        {
            var pos1 = new Position(1, 5);
            var pos2 = new Position(13, 2);
            Assert.AreEqual(153, Utilities.CalculateEnergy(pos1, pos2));
        }

        [TestMethod]
        public void CalculateDistanceTest1()
        {
            var pos1 = new Position(0, 0);
            var pos2 = new Position(0, 0);
            Assert.AreEqual(Math.Sqrt(0), Utilities.CalculateDistance(pos1, pos2));
        }

        [TestMethod]
        public void CalculateDistanceTest2()
        {
            var pos1 = new Position(1, 5);
            var pos2 = new Position(13, 2);
            Assert.AreEqual(Math.Sqrt(153), Utilities.CalculateDistance(pos1, pos2));
        }

        [TestMethod]
        public void FindNearestStationTest1()
        {
            var robots = new List<Robot.Common.Robot>
            {
                new Robot.Common.Robot() {Energy = 1000, Position = new Position(4, 1)}
            };

            var stations = new List<EnergyStation>
            {
                new EnergyStation() {Energy = 1000, Position = new Position(2, 2), RecoveryRate = 1},
                new EnergyStation() {Energy = 1000, Position = new Position(10, 3), RecoveryRate = 1},
                new EnergyStation() {Energy = 1000, Position = new Position(1, 2), RecoveryRate = 1},
                new EnergyStation() {Energy = 1000, Position = new Position(90, 90), RecoveryRate = 1}
            };

            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = stations
            };

            Assert.AreEqual(stations[0].Position, Utilities.FindNearestFreeStation(robots[0], map, robots));
        }

        [TestMethod]
        public void FindNearestStationTest2()
        {
            var robots = new List<Robot.Common.Robot>
            {
                new Robot.Common.Robot() {Energy = 1000, Position = new Position(4, 1)}
            };

            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = new List<EnergyStation>()
            };

            Assert.IsNull(Utilities.FindNearestFreeStation(robots[0], map, robots));
        }

        [TestMethod]
        public void IsStationFreeTest1()
        {
            var robots = new List<Robot.Common.Robot>
            {
                new Robot.Common.Robot() {Energy = 1000, Position = new Position(0, 0)},
                new Robot.Common.Robot() {Energy = 1000, Position = new Position(4, 2)}
            };

            var stations = new List<EnergyStation>
            {
                new EnergyStation() {Energy = 1000, Position = new Position(4, 1), RecoveryRate = 1}
            };

            Assert.IsTrue(Utilities.IsStationFree(stations[0], robots[0], robots));
        }

        [TestMethod]
        public void IsStationFreeTest2()
        {
            var robots = new List<Robot.Common.Robot>
            {
                new Robot.Common.Robot() {Energy = 1000, Position = new Position(0, 0)},
                new Robot.Common.Robot() {Energy = 1000, Position = new Position(4, 1)}
            };

            var stations = new List<EnergyStation>
            {
                new EnergyStation() {Energy = 1000, Position = new Position(4, 1), RecoveryRate = 1}
            };

            Assert.IsFalse(Utilities.IsStationFree(stations[0], robots[0], robots));
        }

        [TestMethod]
        public void IsCellFreeTest1()
        {
            var robots = new List<Robot.Common.Robot>
            {
                new Robot.Common.Robot() {Energy = 1000, Position = new Position(0, 0)},
                new Robot.Common.Robot() {Energy = 1000, Position = new Position(2, 2)}
            };
            
            Assert.IsTrue(Utilities.IsCellFree(new Position(2, 3), robots[0], robots));
        }

        [TestMethod]
        public void IsCellFreeTest2()
        {
            var robots = new List<Robot.Common.Robot>
            {
                new Robot.Common.Robot() {Energy = 1000, Position = new Position(0, 0)},
                new Robot.Common.Robot() {Energy = 1000, Position = new Position(2, 3)}
            };

            Assert.IsFalse(Utilities.IsCellFree(new Position(2, 3), robots[0], robots));
        }
    }
}
