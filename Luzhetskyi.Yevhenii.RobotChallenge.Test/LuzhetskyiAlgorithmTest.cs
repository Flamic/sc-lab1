﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Robot.Common;

namespace Luzhetskyi.Yevhenii.RobotChallenge.Test
{
    [TestClass]
    public class LuzhetskyiAlgorithmTest
    {
        [TestMethod]
        public void AuthorTest1()
        {
            var algo = new LuzhetskyiAlgorithm();
            Assert.IsNotNull(algo.Author);
        }

        [TestMethod]
        public void AuthorTest2()
        {
            var algo = new LuzhetskyiAlgorithm();
            Assert.AreEqual(2, algo.Author.Trim().Split(' ').Length);
        }

        [TestMethod]
        public void DoStepTest1()
        {
            var algo = new LuzhetskyiAlgorithm();
            var robots = new List<Robot.Common.Robot>
            {
                new Robot.Common.Robot() {Energy = 100, Position = new Position(2, 2)}
            };

            var stations = new List<EnergyStation>
            {
                new EnergyStation() {Energy = 1000, Position = new Position(2, 3), RecoveryRate = 1},
                new EnergyStation() {Energy = 1000, Position = new Position(4, 1), RecoveryRate = 1}
            };

            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = stations
            };
            algo.DoStep(robots, 0, map)?.Apply(robots, 0, map);

            Assert.AreEqual(new Position(2, 3), robots[0].Position);
        }

        [TestMethod]
        public void DoStepTest2()
        {
            var algo = new LuzhetskyiAlgorithm();
            var robots = new List<Robot.Common.Robot>
            {
                new Robot.Common.Robot() {Energy = 1000, Position = new Position(2, 2)}
            };

            var stations = new List<EnergyStation>
            {
                new EnergyStation() {Energy = 1000, Position = new Position(2, 3), RecoveryRate = 1},
                new EnergyStation() {Energy = 1000, Position = new Position(4, 1), RecoveryRate = 1}
            };

            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = stations
            };
            var com = algo.DoStep(robots, 0, map);
            
            Assert.IsTrue(com is CreateNewRobotCommand);
        }

        [TestMethod]
        public void MoveTest()
        {
            var algo = new LuzhetskyiAlgorithm();
            var robots = new List<Robot.Common.Robot>
            {
                new Robot.Common.Robot() {Energy = 1000, Position = new Position(99, 0)}
            };

            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = new List<EnergyStation>()
            };
            var com = new MoveCommand() { NewPosition = new Position(0, 0) };
            com.Apply(robots, 0, map);

            Assert.AreEqual(999, robots[0].Energy);
        }

        [TestMethod]
        public void DoStepTest3()
        {
            var algo = new LuzhetskyiAlgorithm();
            var robots = new List<Robot.Common.Robot>
            {
                new Robot.Common.Robot() {Energy = 100, Position = new Position(2, 2)}
            };

            var stations = new List<EnergyStation>
            {
                new EnergyStation() {Energy = 1000, Position = new Position(0, 0), RecoveryRate = 1}
            };

            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = stations
            };
            Variant.Initialize(2);
            algo.DoStep(robots, 0, map).Apply(robots, 0, map);
            
            Assert.AreEqual(140, robots[0].Energy);
        }

        [TestMethod]
        public void DoStepTest4()
        {
            var robots = new List<Robot.Common.Robot>
            {
                new Robot.Common.Robot() {Energy = 100, Position = new Position(2, 2)}
            };

            var stations = new List<EnergyStation>
            {
                new EnergyStation() {Energy = 1000, Position = new Position(0, 0), RecoveryRate = 1},
                new EnergyStation() {Energy = 1000, Position = new Position(4, 4), RecoveryRate = 1}
            };

            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = stations
            };
            Variant.Initialize(2);
            new CollectEnergyCommand().Apply(robots, 0, map);

            Assert.AreEqual(180, robots[0].Energy);
        }
        /*[TestMethod]
        public void MoveTest1()
        {
            var algo = new LuzhetskyiAlgorithm();
            var robots = new List<Robot.Common.Robot>
            {
                new Robot.Common.Robot() {Energy = 1000, Position = new Position(2, 2)}
            };

            var map = new Map()
            { 
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = new List<EnergyStation>()
            };
            var com = new MoveCommand() { NewPosition = new Position(0, 0) };
            com.Apply(robots, 0, map);

            Assert.AreEqual(new Position(0, 0), robots[0].Position);
        }

        [TestMethod]
        public void MoveTest2()
        {
            var algo = new LuzhetskyiAlgorithm();
            var robots = new List<Robot.Common.Robot>
            {
                new Robot.Common.Robot() {Energy = 1000, Position = new Position(2, 2)},
                new Robot.Common.Robot() {Energy = 1000, Position = new Position(4, 4)}
            };

            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = new List<EnergyStation>()
            };
            var com = new MoveCommand() { NewPosition = new Position(4, 4) };
            com.Apply(robots, 0, map);

            Assert.AreEqual(982, robots[0].Energy);
            Assert.AreEqual(new Position(4, 4), robots[0].Position);
        }

        [TestMethod]
        public void MoveTest3()
        {
            var algo = new LuzhetskyiAlgorithm();
            var robots = new List<Robot.Common.Robot>
            {
                new Robot.Common.Robot() {Energy = 1000, Position = new Position(0, 0)}
            };

            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = new List<EnergyStation>()
            };
            var com = new MoveCommand() { NewPosition = new Position(-1, 0) };
            com.Apply(robots, 0, map);
            
            Assert.AreEqual(new Position(0, 0), robots[0].Position);
        }

        [TestMethod]
        public void est3()
        {
            var algo = new LuzhetskyiAlgorithm();
            var robots = new List<Robot.Common.Robot>
            {
                new Robot.Common.Robot() {Energy = 1000, Position = new Position(0, 0)}
            };

            var map = new Map()
            {
                MaxPozition = new Position(100, 100),
                MinPozition = new Position(0, 0),
                Stations = new List<EnergyStation>()
            };
            var com = new MoveCommand() { NewPosition = new Position(-1, 0) };
            com.Apply(robots, 0, map);

            Assert.AreEqual(new Position(0, 0), robots[0].Position);
        }*/
    }
}
